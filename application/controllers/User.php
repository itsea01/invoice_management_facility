<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Sagar Mali
 * @version : 1.1
 * @since : 21 Oct 2018
 */
class User extends BaseController {

    /**
     * This is default constructor of the class
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        $this->isLoggedIn();
        error_reporting(E_ERROR | E_PARSE);
        //without index.php
        // $this->redirect_key = '';
        // $this->form_submit_redirect_key = 'user/';
        //with index.php
        $this->redirect_key = 'index.php/user/';
        $this->form_submit_redirect_key = '';
        $this->report_types = array('Category Based Invoices', 'Outstanding Invoices', 'Invoice Transactions');
        $this->currancy = array('USD', 'EUR', 'NAF');
        $this->categories = array('Local', 'International');
        $this->status_types = array('Cancelled', 'Double Entry', 'Entered', 'Filed', 'Misplaced', 'Paid', 'Rejected', 'On Hold');
    }

    /**
     * This function used to load the first screen of the user
     */
    public function index() {
        redirect($this->redirect_key . 'dashboard');
    }

    /* Only Views Start */

    function invoice($id = 0) {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');

            $departments = array();
            $vendors = array();
            $send_tos = array();

            // get all departments
            $table = 'ims_department';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $departments[$ro->id] = $ro->department;
                }
            }
            asort($departments);

            //get vendors
            $table = 'ims_vendors';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $vendors[$ro->id] = $ro->name;
                }
            }
            asort($vendors);

            // get $street_names
            $table = 'ims_send_to';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $send_tos[$ro->id] = $ro->fname . ' ' . $ro->lname;
                }
            }

            asort($send_tos);


            $data['vendors'] = $vendors;
            $data['reasons'] = $reasons;
            $data['departments'] = $departments;
            $data['send_tos'] = $send_tos;

            $data['currancy'] = $this->currancy;
            $data['categories'] = $this->categories;
            $data['status_types'] = $this->status_types;


            $this->global['pageTitle'] = 'Invoice Management : Invoice Manager';

            if ($id > 0) {
                $table = 'ims_invoices';
                $where = array('id' => $id);
                $data['invoice_data'] = $this->user_model->getData($table, $where)->row();
                $data['edit_breakdown'] = 1;
                $data['edit_form'] = $id;
                $this->global['dashboardTitle'] = 'Edit Invoice';
            } else {
                $this->global['dashboardTitle'] = 'Add Invoice';
            }
            $data['redirect_key'] = $this->redirect_key;
            $this->loadViews("manageInvoice", $this->global, $data, NULL);
        }
    }

    function doAddInvoice() {
//        $insertInfo['call_id'] = $this->input->post('call_id');
        $insertInfo['invoice_no'] = $this->input->post('invoice_no');
        $insertInfo['form_no'] = $this->input->post('form_no');
        $insertInfo['vendor_id'] = $this->input->post('vendor_id');
        $insertInfo['currancy'] = $this->input->post('currancy');
        $insertInfo['invoice_amount'] = $this->input->post('invoice_amount');
        $insertInfo['category'] = $this->input->post('category');
        $insertInfo['send_to_id'] = $this->input->post('send_to_id');
        $insertInfo['note'] = $this->input->post('note');
		

        $i_dt = $this->input->post('invoice_date');
        if (isset($i_dt) && $i_dt != '' && $this->validateDate($i_dt)) {
            $insertInfo['invoice_date'] = date('Y-m-d', strtotime($i_dt));
        } else {
            $insertInfo['invoice_date'] = '0000-00-00';
        }
        $e_dt = $this->input->post('date_entered');
        if (isset($e_dt) && $e_dt != '' && $this->validateDate($e_dt)) {
            $insertInfo['date_entered'] = date('Y-m-d', strtotime($e_dt));
        } else {
            $insertInfo['date_entered'] = '0000-00-00';
        }
        $s_dt = $this->input->post('send_to_date');
        if (isset($s_dt) && $s_dt != '' && $this->validateDate($s_dt)) {
            $insertInfo['send_to_date'] = date('Y-m-d', strtotime($s_dt));
        } else {
            $insertInfo['send_to_date'] = '0000-00-00';
        }
        $r_dt = $this->input->post('date_returned');
        if (isset($r_dt) && $r_dt != '' && $this->validateDate($r_dt)) {
            $insertInfo['date_returned'] = date('Y-m-d', strtotime($r_dt));
        } else {
            $insertInfo['date_returned'] = '0000-00-00';
        }

        $insertInfo['status'] = $this->input->post('status');
        $insertInfo['remark'] = $this->input->post('remark');



        $table = 'ims_invoices';

        $edit_form = $this->input->post('edit_form');

        if ($edit_form > 0) {
            $result = $this->user_model->updateData($table, $edit_form, $insertInfo);
            if ($result == true) {
                $this->session->set_flashdata('success', 'Invoice updated successfully');
            } else {
                $this->session->set_flashdata('error', 'Invoice updation failed');
            }
        } else {
            $insertInfo['createdDtm'] = date('Y-m-d H:i:s');
            $insertInfo['created_by'] = $_SESSION['userId'];

            $result = $this->user_model->addData($table, $insertInfo);
            if ($result == true) {
                $this->session->set_flashdata('success', 'Invoice created successfully');
            } else {
                $this->session->set_flashdata('error', 'Invoice craetion failed');
            }
        }

        redirect($this->redirect_key . 'dashboard');
    }

    function dashboard() {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');

            $departments = array();
            $reasons = array();
            $vendors = array();
            $send_tos = array();


            // get all departments
            $table = 'ims_department';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $departments[$ro->id] = $ro->department;
                }
            }



            //get vendors
            $table = 'ims_vendors';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $vendors[$ro->id] = $ro->name;
                }
            }

            // get $street_names
            $table = 'ims_send_to';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $send_tos[$ro->id] = $ro->fname . ' ' . $ro->lname;
                }
            }

            $data['vendors'] = $vendors;

            $data['departments'] = $departments;
            $data['send_tos'] = $send_tos;

            $table = 'ims_invoices';
            $data['invoices'] = $this->user_model->getData($table);

            $this->global['pageTitle'] = 'Invoice Management :  Add/Upate/Delete Breakdowns';
            $data['redirect_key'] = $this->redirect_key;
            $this->loadViews("dashboard", $this->global, $data, NULL);
        }
    }

    function reports() {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');

            $departments = array();

            $vendors = array();
            $street_names = array();


            // get all departments
            $table = 'ims_department';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $departments[$ro->id] = $ro->department;
                }
            }



            //get vendors
            $table = 'ims_vendors';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $vendors[$ro->id] = $ro->name;
                }
            }

            // get $street_names
            $table = 'ims_send_to';
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $street_names[$ro->id] = $ro->street_name;
                }
            }

            $data['vendors'] = $vendors;

            $data['departments'] = $departments;
            $data['street_names'] = $street_names;

            $data['report_types'] = $this->report_types;
            $data['categories'] = $this->categories;
            $data['currancies'] = $this->currancy;



            $report_type = $this->input->post('report_type');
            $from_date = $this->input->post('from_date');
            $to_date = $this->input->post('to_date');


            $report_category = $this->input->post('report_category');
            $report_outstandings = $this->input->post('report_outstandings');
            $report_currancy = $this->input->post('report_currancy');


            if ($report_type == '') {
                $report_type = 'Category Based Invoices';
            }
            if ($from_date == '') {
                $from_date = date('Y-m-d');
            }
            if ($to_date == '') {
                $to_date = date('Y-m-d');
            }

            $whereInfo = array('invoice_date >=' => $from_date, 'invoice_date <=' => $to_date);

            if ($report_type == 'Category Based Invoices') {
                if ($report_category == '') {
                    $report_category = 'Local';
                }
                $whereInfo['category'] = $report_category;
                $this->global['pageTitle'] = $report_category . ' Completed Invoices : ' . $from_date . ' - ' . $to_date;
            }

            if ($report_type == 'Outstanding Invoices') {
                if ($report_outstandings == 1) {
                    $whereInfo = array('status !=' => 'paid');
                    $this->global['pageTitle'] = 'Outstanding Invoices : All';
                } else {
                    $this->global['pageTitle'] = 'Outstanding Invoices : ' . $from_date . ' - ' . $to_date;
                }
                $whereInfo['status !='] = 'paid';
            }
            if ($report_type == 'Invoice Transactions') {
                $whereInfo['currancy'] = $report_currancy;
                $this->global['pageTitle'] = 'Invoice Transactions : ' . $from_date . ' - ' . $to_date;
            }




            $table = 'ims_invoices';
            $data['reportData'] = $this->user_model->getData($table, $whereInfo);
            $data['last_query'] = $this->db->last_query();


            $data['report_type'] = $report_type;
            $data['from_date'] = $from_date;
            $data['to_date'] = $to_date;
            $data['report_category'] = $report_category;
            $data['report_currancy'] = $report_currancy;
            $data['report_outstandings'] = $report_outstandings;

            $data['redirect_key'] = $this->redirect_key;
            $this->loadViews("reports", $this->global, $data, NULL);
        }
    }

    function trackings() {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['redirect_key'] = $this->redirect_key;
            $this->global['pageTitle'] = 'Trackings || Invoice Management';
            $data['invoices'] = $this->user_model->getData('ims_invoices');
            $data['sendtos'] = $this->user_model->getData('ims_send_to');
            $table = 'ims_send_to';
            $vendors = array();
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $vendors[$ro->id] = $ro->fname . ' ' . $ro->lname;
                }
            }
            $data['vendors'] = $vendors;
            $data['trackings'] = $this->user_model->getData('ims_trackings');
            $this->loadViews("trackings", $this->global, $data, NULL);
        }
    }

    function vendors() {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['redirect_key'] = $this->redirect_key;
            $this->global['pageTitle'] = 'Vendors || Invoice Management';
            $data['vendors'] = $this->user_model->getVendors()->result();
            $this->loadViews("vendors", $this->global, $data, NULL);
        }
    }

    function settings() {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $this->global['pageTitle'] = 'Invoice Management :  Settings';
            $data['send_tos'] = $this->user_model->getData('ims_send_to');
            $data['invoices'] = $this->user_model->getData('ims_invoices');
            $data['departments'] = $this->user_model->getData('ims_department');

            $table = 'ims_department';
            $departments_dp = array();
            $res = $this->user_model->getData($table);
            if (!empty($res) && $res->num_rows() > 0) {
                foreach ($res->result() as $ro) {
                    $departments_dp[$ro->id] = $ro->department;
                }
            }

            $data['departments_dp'] = $departments_dp;

            $data['redirect_key'] = $this->redirect_key;
            $this->loadViews("settings", $this->global, $data, NULL);
        }
    }

    function userListing() {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $searchText = $this->security->xss_clean($this->input->post('searchText'));
            $data['searchText'] = $searchText;

            $this->load->library('pagination');

            $count = $this->user_model->userListingCount($searchText);

            $returns = $this->paginationCompress("userListing/", $count, 10);

            $data['userRecords'] = $this->user_model->userListing($searchText, $returns["page"], $returns["segment"]);


            $this->global['pageTitle'] = 'Invoice Management : User Listing';
            $data['redirect_key'] = $this->redirect_key;
            $this->loadViews("users", $this->global, $data, NULL);
        }
    }

    function addNew() {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->model('user_model');
            $data['roles'] = $this->user_model->getUserRoles();
            $this->global['pageTitle'] = 'Invoice Management : Add New User';
            $data['redirect_key'] = $this->redirect_key;
            $this->loadViews("addNew", $this->global, $data, NULL);
        }
    }

    function editOld($userId = NULL) {
        if ($this->isAdmin() == TRUE || $userId == 1) {
            $this->loadThis();
        } else {
            if ($userId == null) {
                redirect($this->redirect_key . 'userListing');
            }

            $data['roles'] = $this->user_model->getUserRoles();
            $data['userInfo'] = $this->user_model->getUserInfo($userId);

            $this->global['pageTitle'] = 'Invoice Management : Edit User';
            $data['redirect_key'] = $this->redirect_key;
            $this->loadViews("editOld", $this->global, $data, NULL);
        }
    }

    function pageNotFound() {
        $this->global['pageTitle'] = 'Invoice Management : 404 - Page Not Found';
        $data['redirect_key'] = $this->redirect_key;
        $this->loadViews("404", $this->global, NULL, NULL);
    }

    function profile($active = "details") {
        $data["userInfo"] = $this->user_model->getUserInfoWithRole($this->vendorId);
        $data["active"] = $active;
        $data['redirect_key'] = $this->redirect_key;

        $this->global['pageTitle'] = $active == "details" ? 'Invoice Management : My Profile' : 'Invoice Management : Change Password';
        $this->loadViews("profile", $this->global, $data, NULL);
    }

    function loginHistory($userId = NULL) {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $userId = ($userId == NULL ? 0 : $userId);

            $searchText = $this->input->post('searchText');
            $fromDate = $this->input->post('fromDate');
            $toDate = $this->input->post('toDate');

            $data["userInfo"] = $this->user_model->getUserInfoById($userId);

            $data['searchText'] = $searchText;
            $data['fromDate'] = $fromDate;
            $data['toDate'] = $toDate;

            $this->load->library('pagination');

            // $count = $this->user_model->loginHistoryCount($userId, $searchText, $fromDate, $toDate);
            // $returns = $this->paginationCompress("loginHistoy/" . $userId . "/", $count, 10, 3);

            $data['userRecords'] = $this->user_model->loginHistory($userId, $searchText, $fromDate, $toDate);
            // die($this->db->last_query());
            $this->global['pageTitle'] = 'Invoice Management : User Login History';
            $data['redirect_key'] = $this->redirect_key;

            $this->loadViews("loginHistory", $this->global, $data, NULL);
        }
    }

    /* Only Views End */


    /* Ajax Calls start */

    function getDepartmentDp() {
        $dept_id = $this->input->post('curr_dept');
        $this->db->select('*');
        $this->db->from('ims_department');
        $result = $this->db->get()->result();
        $department_dp = '';
        if (!empty($result)) {
            foreach ($result as $row) {
                $selected = "";
                if ($dept_id == $row->id) {
                    $selected = ' selected';
                }
                $department_dp .= "<option value=" . $row->id . "  " . $selected . ">" . $row->department . "</option>";
            }
        }
        echo json_encode(array('department_dp' => $department_dp));
        exit();
    }

    function getSend_ToDetails() {
        $send_to_id = $this->input->post('send_to_id');
        $this->db->select('id,fname,lname');
        $this->db->from('ims_send_to');
        $this->db->where('id', $send_to_id);
        $result = $this->db->get()->row();

        if (!empty($result)) {
            echo json_encode(array('id' => $result->id, 'fname' => $result->fname, 'lname' => $result->lname));
        }

        exit();
    }

    function getReportsDropDown() {
        $key = $this->input->post('key');

        $categories = $this->categories;
        $currancies = $this->currancy;

        $drop_down = '';
        if ($key == 'Category Based Invoices') {
            $drop_down .= '<select class="form-control" name="report_category">';
            foreach ($categories as $cat) {
                $drop_down .= "<option value=" . $cat . ">" . $cat . "</option>";
            }
            $drop_down .= '</select>';
        } else if ($key == 'Outstanding Invoices') {
            $drop_down .= '<input type="checkbox" name="report_outstandings" value="1"> Show All';
        } else if ($key == 'Invoice Transactions') {
            $drop_down .= '<select class="form-control" name="report_currancy">';
            foreach ($currancies as $curr) {
                $drop_down .= "<option value=" . $curr . ">" . $curr . "</option>";
            }
            $drop_down .= '</select>';
        }
        echo json_encode(array('drop_down' => $drop_down));
        exit();
    }

    function getSendToDetails() {
        $street_name_id = $this->input->post('street_name_id');
        $this->db->select('*');
        $this->db->from('ims_send_to');
        $this->db->where('id', $street_name_id);
        $query = $this->db->get()->row();
        echo json_encode(array('id' => $street_name_id, 'street_name' => $query->street_name, 'department_id' => $query->department_id));
        exit();
    }

    function getDepartmentDetails() {
        $department_id = $this->input->post('department_id');
        $this->db->select('*');
        $this->db->from('ims_department');
        $this->db->where('id', $department_id);
        $query = $this->db->get()->row();
        echo json_encode(array('id' => $department_id, 'department' => $query->department));
        exit();
    }

    function deleteRow() {
        if (0) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $del_id = $this->input->post('del_id');
            $del_tbl = $this->input->post('del_tbl');

            $result = $this->user_model->deleteRow($del_id, $del_tbl);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    function deleteUser() {
        if ($this->isAdmin() == TRUE) {
            echo(json_encode(array('status' => 'access')));
        } else {
            $userId = $this->input->post('userId');
            $userInfo = array('isDeleted' => 1, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));

            $result = $this->user_model->deleteUser($userId, $userInfo);

            if ($result > 0) {
                echo(json_encode(array('status' => TRUE)));
            } else {
                echo(json_encode(array('status' => FALSE)));
            }
        }
    }

    function checkEmailExists() {

        $userId = $this->input->post("userId");
        $email = $this->input->post("email");

        if (empty($userId)) {
            $result = $this->user_model->checkEmailExists($email);
        } else {
            $result = $this->user_model->checkEmailExists($email, $userId);
        }

        if (empty($result)) {
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
        exit();
    }

    function getVendorDetails() {
        $tech_id = $this->input->post('vendor_id');
        $this->db->select('*');
        $this->db->from('ims_vendors');
        $this->db->where('id', $tech_id);
        $query = $this->db->get()->row();
        echo json_encode(array('id' => $tech_id, 'name' => $query->name, 'phone' => $query->phone, 'email' => $query->email, 'location' => $query->location, 'mobile' => $query->name, 'ability' => $query->ability));
        exit();
    }

    function getTrakcingDetails() {
        $tracking_id = $this->input->post('tracking_id');
        $this->db->select('*');
        $this->db->from('ims_trackings');
        $this->db->where('id', $tracking_id);
        $query = $this->db->get()->row();
        $invoice_data = $query->invoice_id . '_' . $query->invoice_no . '_' . $query->invoice_form;
        echo json_encode(array('id' => $tracking_id, 'send_to_id' => $query->send_to_id, 'send_to_date' => $query->send_to_date, 'returned_date' => $query->returned_date, 'invoice_data' => $invoice_data));
        exit();
    }

    /* Ajax Calls end */

    /* form submit start */

    function doAddDepartment() {
        if (0) {
            $this->loadThis();
        } else {
            $insertInfo['department'] = $this->input->post('department');
            $insertInfo['createdDtm'] = date('Y-m-d H:i:s');
            $result = $this->user_model->addNewDepartment($insertInfo);
            if ($result > 0) {
                $this->session->set_flashdata('success', 'New Department created successfully');
            } else {
                $this->session->set_flashdata('error', 'Department creation failed');
            }
            redirect($this->redirect_key . 'settings');
        }
    }

    function doAddReason() {
        if (0) {
            $this->loadThis();
        } else {
            $insertInfo['reason'] = $this->input->post('reason');
            $insertInfo['createdDtm'] = date('Y-m-d H:i:s');
            $result = $this->user_model->addNewReason($insertInfo);
            if ($result > 0) {
                $this->session->set_flashdata('success', 'Reason created successfully');
            } else {
                $this->session->set_flashdata('error', 'Reason creation failed');
            }
            redirect($this->redirect_key . 'settings');
        }
    }

    function doAddSendTo() {
        if (0) {
            $this->loadThis();
        } else {
            $insertInfo['fname'] = $this->input->post('fname');
            $insertInfo['lname'] = $this->input->post('lname');
            $insertInfo['department_id'] = $this->input->post('department');
            $insertInfo['createdDtm'] = date('Y-m-d H:i:s');
            $result = $this->user_model->addNewSendTo($insertInfo);
            if ($result > 0) {
                $this->session->set_flashdata('success', 'SendTo created successfully');
            } else {
                $this->session->set_flashdata('error', 'SendTo creation failed');
            }
            redirect($this->redirect_key . 'settings');
        }
    }

    function doAddTracking() {
        if (0) {
            $this->loadThis();
        } else {
            $invoice_data = explode('_', $this->input->post('invoice_data'));
            $insertInfo['send_to_id'] = $this->input->post('send_to_id');
            $insertInfo['send_to_date'] = $this->input->post('send_to_date');
            $insertInfo['returned_date'] = $this->input->post('returned_date');
            $insertInfo['invoice_id'] = $invoice_data[0];
            $insertInfo['invoice_no'] = $invoice_data[1];
            $insertInfo['invoice_form'] = $invoice_data[2];

            $insertInfo['createdDtm'] = date('Y-m-d H:i:s');

            $result = $this->user_model->addNewTracking($insertInfo);

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Tracking created successfully');
            } else {
                $this->session->set_flashdata('error', 'Tracking creation failed');
            }
            redirect($this->redirect_key . 'trackings');
        }
    }

    function doUpdateTracking() {
        if (0) {
            $this->loadThis();
        } else {
            $invoice_data = explode('_', $this->input->post('invoice_data'));
            $whereInfo['id'] = $this->input->post('tracking_id');
            $insertInfo['send_to_id'] = $this->input->post('send_to_id');
            $insertInfo['send_to_date'] = $this->input->post('send_to_date');
            $insertInfo['returned_date'] = $this->input->post('returned_date');
            $insertInfo['invoice_id'] = $invoice_data[0];
            $insertInfo['invoice_no'] = $invoice_data[1];
            $insertInfo['invoice_form'] = $invoice_data[2];

            //  $insertInfo['createdDtm'] = date('Y-m-d H:i:s');


            $table = 'ims_trackings';
            $result = $this->user_model->updateTable($table, $insertInfo, $whereInfo);

            if ($result > 0) {
                $this->session->set_flashdata('success', 'Tracking updated successfully');
            } else {
                $this->session->set_flashdata('error', 'Tracking updation failed');
            }
            redirect($this->redirect_key . 'trackings');
        }
    }

    function doUpdateDepartment() {
        if (0) {
            $this->loadThis();
        } else {
            $updateInfo['department'] = $this->input->post('department');
            $whereInfo['id'] = $this->input->post('department_id');
            $table = 'ims_department';
            $result = $this->user_model->updateTable($table, $updateInfo, $whereInfo);
            if ($result > 0) {
                $this->session->set_flashdata('success', 'Department updated successfully');
            } else {
                $this->session->set_flashdata('error', 'Department updation failed');
            }
            redirect($this->redirect_key . 'settings');
        }
    }

    function doUpdateSendTo() {
        if (0) {
            $this->loadThis();
        } else {
            $whereInfo['id'] = $this->input->post('send_to_id');
            $updateInfo['fname'] = $this->input->post('fname');
            $updateInfo['lname'] = $this->input->post('lname');
            $updateInfo['department_id'] = $this->input->post('department_id');
            $table = 'ims_send_to';
            $result = $this->user_model->updateTable($table, $updateInfo, $whereInfo);
            if ($result > 0) {
                $this->session->set_flashdata('success', 'SendTo updated successfully');
            } else {
                //  echo $this->db->last_query();
                //   die();
                $this->session->set_flashdata('error', 'SendTo updation failed');
            }
            redirect($this->redirect_key . 'settings');
        }
    }

    function addNewUser() {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fname', 'Full Name', 'trim|required|max_length[128]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[20]');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'trim|required|matches[password]|max_length[20]');
            $this->form_validation->set_rules('role', 'Role', 'trim|required|numeric');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|min_length[10]');

            if ($this->form_validation->run() == FALSE) {
                $this->addNew();
            } else {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));

                $userInfo = array('email' => $email, 'password' => getHashedPassword($password), 'roleId' => $roleId, 'name' => $name,
                    'mobile' => $mobile, 'createdBy' => $this->vendorId, 'createdDtm' => date('Y-m-d H:i:s'));

                $this->load->model('user_model');
                $result = $this->user_model->addNewUser($userInfo);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New User created successfully');
                } else {
                    $this->session->set_flashdata('error', 'User creation failed');
                }

                redirect($this->redirect_key . 'addNew');
            }
        }
    }

    function addNewEmployee() {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fname', 'Full Name', 'trim|required|max_length[128]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|min_length[10]');

            if ($this->form_validation->run() == FALSE) {
                $this->addNewEmp();
            } else {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $emp_id = $this->input->post('emp_id');

                $userInfo = array('email' => $email, 'name' => $name, 'emp_id' => $emp_id, 'mobile' => $mobile, 'createdBy' => $this->vendorId, 'createdDtm' => date('Y-m-d H:i:s'));

                $this->load->model('user_model');
                $result = $this->user_model->addNewEmployee($userInfo);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Employee created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Employee creation failed');
                }

                redirect($this->redirect_key . 'addNewEmp');
            }
        }
    }

    function editUser() {
        if ($this->isAdmin() == TRUE) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $userId = $this->input->post('userId');

            $this->form_validation->set_rules('fname', 'Full Name', 'trim|required|max_length[128]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('password', 'Password', 'matches[cpassword]|max_length[20]');
            $this->form_validation->set_rules('cpassword', 'Confirm Password', 'matches[password]|max_length[20]');
            $this->form_validation->set_rules('role', 'Role', 'trim|required|numeric');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|min_length[10]');

            if ($this->form_validation->run() == FALSE) {
                $this->editOld($userId);
            } else {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $password = $this->input->post('password');
                $roleId = $this->input->post('role');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));

                $userInfo = array();

                if (empty($password)) {
                    $userInfo = array('email' => $email, 'roleId' => $roleId, 'name' => $name,
                        'mobile' => $mobile, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));
                } else {
                    $userInfo = array('email' => $email, 'password' => getHashedPassword($password), 'roleId' => $roleId,
                        'name' => ucwords($name), 'mobile' => $mobile, 'updatedBy' => $this->vendorId,
                        'updatedDtm' => date('Y-m-d H:i:s'));
                }

                $result = $this->user_model->editUser($userInfo, $userId);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'User updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'User updation failed');
                }

                redirect($this->redirect_key . 'userListing');
            }
        }
    }

    function editEmployee() {
        if (0) {
            $this->loadThis();
        } else {
            $this->load->library('form_validation');

            $userId = $this->input->post('userId');

            $this->form_validation->set_rules('fname', 'Full Name', 'trim|required|max_length[128]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[128]');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|min_length[10]');

            if ($this->form_validation->run() == FALSE) {
                $this->editOld($userId);
            } else {
                $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
                $email = strtolower($this->security->xss_clean($this->input->post('email')));
                $emp_id = $this->input->post('emp_id');
                $mobile = $this->security->xss_clean($this->input->post('mobile'));

                $userInfo = array();


                $userInfo = array('email' => $email, 'name' => $name, 'emp_id' => $emp_id,
                    'mobile' => $mobile, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));


                $result = $this->user_model->editEmployee($userInfo, $userId);

                if ($result == true) {
                    $this->session->set_flashdata('success', 'Employee updated successfully');
                } else {
                    $this->session->set_flashdata('error', 'Employee updation failed');
                }

                redirect($this->redirect_key . 'empListing');
            }
        }
    }

    function doAddAbsenceType() {
        $absence_type = strtoupper($this->input->post('absence_type'));
        $color = $this->input->post('color');
        $type_desc = $this->input->post('type_desc');

        $typeInfo = array('color' => $color, 'type' => $absence_type, 'type_desc' => $type_desc, 'createdDtm' => date('Y-m-d H:i:s'));

        $this->load->model('user_model');
        $result = $this->user_model->addAbsenceType($typeInfo);
        redirect($this->redirect_key . 'absenceTypes');
    }

    function profileUpdate($active = "details") {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('fname', 'Full Name', 'trim|required|max_length[128]');
        $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|min_length[10]');

        if ($this->form_validation->run() == FALSE) {
            $this->profile($active);
        } else {
            $name = ucwords(strtolower($this->security->xss_clean($this->input->post('fname'))));
            $mobile = $this->security->xss_clean($this->input->post('mobile'));

            $userInfo = array('name' => $name, 'mobile' => $mobile, 'updatedBy' => $this->vendorId, 'updatedDtm' => date('Y-m-d H:i:s'));

            $result = $this->user_model->editUser($userInfo, $this->vendorId);

            if ($result == true) {
                $this->session->set_userdata('name', $name);
                $this->session->set_flashdata('success', 'Profile updated successfully');
            } else {
                $this->session->set_flashdata('error', 'Profile updation failed');
            }
            redirect($this->redirect_key . 'profile/' . $active);
        }
    }

    function changePassword($active = "changepass") {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('oldPassword', 'Old password', 'required|max_length[20]');
        $this->form_validation->set_rules('newPassword', 'New password', 'required|max_length[20]');
        $this->form_validation->set_rules('cNewPassword', 'Confirm new password', 'required|matches[newPassword]|max_length[20]');

        if ($this->form_validation->run() == FALSE) {
            $this->profile($active);
        } else {
            $oldPassword = $this->input->post('oldPassword');
            $newPassword = $this->input->post('newPassword');

            $resultPas = $this->user_model->matchOldPassword($this->vendorId, $oldPassword);

            if (empty($resultPas)) {
                $this->session->set_flashdata('nomatch', 'Your old password is not correct');
                redirect($this->redirect_key . 'profile/' . $active);
            } else {
                $usersData = array('password' => getHashedPassword($newPassword), 'updatedBy' => $this->vendorId,
                    'updatedDtm' => date('Y-m-d H:i:s'));

                $result = $this->user_model->changePassword($this->vendorId, $usersData);

                if ($result > 0) {
                    $this->session->set_flashdata('success', 'Password updation successful');
                } else {
                    $this->session->set_flashdata('error', 'Password updation failed');
                }

                redirect($this->redirect_key . 'profile/' . $active);
            }
        }
    }

    function doAddVendor() {
        $name = $this->input->post('name');
        $location = $this->input->post('location');
        $mobile = $this->input->post('mobile');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $typeInfo = array('name' => $name, 'location' => $location, 'phone' => $phone, 'mobile' => $mobile, 'email' => $email, 'createdDtm' => date('Y-m-d H:i:s'));
        $this->load->model('user_model');
        $this->user_model->addVendor($typeInfo);
        redirect($this->redirect_key . 'vendors');
    }

    function validateDate($date, $format = 'Y-m-d') {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    function doUpdateVendor() {
        $name = $this->input->post('name');
        $location = $this->input->post('location');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $mobile = $this->input->post('mobile');
        $tech_id = $this->input->post('tech_id');
        $typeInfo = array('name' => $name, 'location' => $location, 'phone' => $phone, 'email' => $email, 'mobile' => $mobile);
        $where = array('id' => $tech_id);
        $this->load->model('user_model');
        $this->user_model->updateVendor($typeInfo, $where);
        redirect($this->redirect_key . 'vendors');
    }

    /* form submit end */
}

?>