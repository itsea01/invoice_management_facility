<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url(); ?>assets/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>assets/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!--<link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />-->
<!-- END PAGE LEVEL PLUGINS -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-book"></i> Reports
            <small>Reports are generated on basis of <b>Invoice Date</b></small>
        </h1>
        <div class="col-md-3 pull-right right">
            <?php
            $error = $this->session->flashdata('error');
            if ($error) {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
            <?php } ?>
            <?php
            $success = $this->session->flashdata('success');
            if ($success) {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php } ?>
        </div>
    </section>
    <style>
        .width50{
        }
        .modal-dialog{
            width: 900px;
            height: 810px;
        }
        .modal-content{
            height: 100%;
        }
        .total_td{
            text-align: right;
            font-weight: bold;
        }
        input[type="checkbox"]{
            height: 25px;
            width: 25px;
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <?php
                //  echo $last_query;
                ?>
                <form action="" method="post">


                    <div class="form-group col-md-3">
                        <select class="form-control report_type" name="report_type">
                            <?php
                            foreach ($report_types as $report) {
                                $selected = ' ';
                                if ($report == $report_type) {
                                    $selected = ' Selected';
                                }
                                echo '<option value="' . $report . '" ' . $selected . '>' . $report . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group col-md-2 second_filter">
                        <?php if (isset($report_type) && $report_type == 'Category Based Invoices') { ?>
                            <select class="form-control" name="report_category">
                                <?php
                                foreach ($categories as $category) {
                                    $selected = ' ';
                                    if ($category == $report_category) {
                                        $selected = ' Selected';
                                    }
                                    echo '<option value="' . $category . '" ' . $selected . '>' . $category . '</option>';
                                }
                                ?>
                            </select>
                            <?php
                        } else if (isset($report_type) && $report_type == 'Outstanding Invoices') {
                            ?>
                            <input type="checkbox" name="report_outstandings" value="1" <?php
                            if (isset($report_outstandings) && $report_outstandings > 0) {
                                echo ' checked';
                            }
                            ?> ><b>Show All Date</b>
                                   <?php
                               } else if (isset($report_type) && $report_type == 'Invoice Transactions') {
                                   ?>
                            <select class="form-control" name="report_currancy">
                                <?php
                                foreach ($currancies as $curr) {
                                    $selected = '';
                                    if ($curr == $report_currancy) {
                                        $selected = ' Selected';
                                    }
                                    echo '<option value="' . $curr . '" ' . $selected . '>' . $curr . '</option>';
                                }
                                ?>
                            </select>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="form-group col-md-1">
                        <label for="date_range">Invoice Date Range:</label>
                    </div>
                    <div class="form-group col-md-2">
                        <input name="from_date" value="<?php echo $from_date; ?>" placeholder="From Date" type="text" class="form-control datepicker" autocomplete="off"> 
                    </div>
                    <div class="form-group col-md-2">
                        <input name="to_date" value="<?php echo $to_date; ?>" placeholder="To Date" type="text" class="form-control datepicker" autocomplete="off"> 
                    </div>
                    <div class="form-group col-md-1">
                        <input type="submit" value="Generate Report" class="btn btn-success" name="generate_report">
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Report</h3>
                        <div class="box-tools">

                        </div>
                    </div><!-- /.box-header -->
                    <div class="portlet light bordered">
                        <div class="box-body table-responsive no-padding">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <i class="icon-settings font-dark"></i>
                                    <span class="caption-subject bold uppercase">Buttons</span>
                                </div>
                                <div class="tools"> </div>
                            </div>

                            <div class="portlet-body">

                                <table class="table table-striped table-bordered table-hover" id="sample_1">
                                    <thead>
                                        <tr>
                                        <th>Invoice No</th>
                                        <th>Invoice Date</th>
                                            <th>Vendor</th>
                                            <th>Date Send To</th>
                                            <th>Date Returned</th>
                                            <th>Date Entered</th>
                                            <?php foreach ($currancies as $curr) { ?>
                                                <th>Amount <?php echo $curr; ?></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $total['USD'] = 0;
                                        $total['EUR'] = 0;
                                        $total['NAF'] = 0;
                                        if (!empty($reportData)) {
                                            foreach ($reportData->result() as $record) {
                                                ?>
                                                <tr>
                                                <td><?php echo $record->invoice_no; ?></td>
                                                <td><?php echo date("Y-m-d", strtotime($record->invoice_date)); ?></td>
                                                    <td><?php echo $vendors[$record->vendor_id]; ?></td>
                                                    <td><?php echo date("Y-m-d", strtotime($record->send_to_date)); ?></td>
                                                    <td><?php echo date("Y-m-d", strtotime($record->date_returned)); ?></td>
                                                    <td><?php echo date("Y-m-d", strtotime($record->date_entered)); ?></td>
                                                    <?php
                                                    if ($record->currancy == 'USD') {
                                                        $total['USD'] = $total['USD'] + $record->invoice_amount;
                                                        echo '<td>' . $record->invoice_amount . '</td>';
                                                    } else {
                                                        echo '<td></td>';
                                                    }
                                                    if ($record->currancy == 'EUR') {
                                                        $total['EUR'] = $total['EUR'] + $record->invoice_amount;
                                                        echo '<td>' . $record->invoice_amount . '</td>';
                                                    } else {
                                                        echo '<td></td>';
                                                    }
                                                    if ($record->currancy == 'NAF') {
                                                        $total['NAF'] = $total['NAF'] + $record->invoice_amount;
                                                        echo '<td>' . $record->invoice_amount . '</td>';
                                                    } else {
                                                        echo '<td></td>';
                                                    }
                                                    ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5" class="total_td">Total</td>
                                            <?php foreach ($currancies as $curr) { ?>
                                                <td><?php
                                                    if ($curr == 'USD') {
                                                        echo $total['USD'];
                                                    } else if ($curr == 'EUR') {
                                                        echo $total['EUR'];
                                                    } else if ($curr == 'NAF') {
                                                        echo $total['NAF'];
                                                    }
                                                    ?></td>
                                            <?php } ?>

                                        </tr>
                                    </tfoot>
                                </table>

                            </div>
                        </div><!-- /.box-body -->
                    </div>
                </div><!-- /.box -->
            </div>
        </div>

    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" charset="utf-8"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />



<script type="text/javascript">
    jQuery(document).ready(function () {
        var table = $('#example').DataTable();

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });

        $('.edit_tech').on('click', function () {
            var tech_id = $(this).data('tech_id');

            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getTechnicianDetails'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    tech_id: tech_id,
                },
                success: function (data) {
                    $('#updateAbsenceModal .name_update').val(data.name);
                    $('#updateAbsenceModal .ability_update').val(data.ability);

                    $('#updateAbsenceModal #technician_id').val(data.id);

//                    $('#updateAbsenceModal #update_node_id').val(data.node_id);
                    $('#updateAbsenceModal').modal('show');
                }
            });
        });

        $('.report_type').on('change', function () {
            var key = $(this).val();
            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getReportsDropDown'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    key: key,
                },
                success: function (data) {
                    $('.second_filter').html();
                    $('.second_filter').html(data.drop_down);
                }
            });
        });


    });
</script>
<!--<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>-->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/datatables/datatable.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!--<script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>-->
<!-- END PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url(); ?>assets/datatables/app.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/datatables/table-datatables-buttons.min.js" type="text/javascript"></script>
