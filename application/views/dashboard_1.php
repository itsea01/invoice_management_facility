<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Break Down
            <small>Add, Edit, Delete, Export</small>
        </h1>
    </section>
    <style>
        .width50{

        }
        .modal-dialog{
            width: 900px;
            height: 810px;
        }
        .modal-content{
            height: 100%;
        }
    </style>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="col-md-3 pull-left left">
                    <?php
                    $error = $this->session->flashdata('error');
                    if ($error) {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $this->session->flashdata('error'); ?>                    
                        </div>
                    <?php } ?>
                    <?php
                    $success = $this->session->flashdata('success');
                    if ($success) {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="form-group pull-right right">
                    <a class="btn btn-primary" href="<?php echo base_url().$redirect_key; ?>breakDown"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Breakdowns</h3>
                        <div class="box-tools">

                        </div>
                    </div><!-- /.box-header -->

                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Division</th>
                                    <th>Region</th>
                                    <th>Caller</th>
                                    <th>Reason</th>
                                    <th>Street Name</th> <!-- house no, street_name, region -->
                                    <th>Technician</th>
                                    <!--<th>Status</th>-->
                                    <th>Created On</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($breakdowns)) {
                                    foreach ($breakdowns->result() as $record) {
                                        ?>
                                        <tr>
                                            <td><?php echo $record->id; ?></td>
                                            <td><?php echo $record->division; ?></td>
                                            <td><?php echo $regions[$record->region_id]; ?></td>
                                            <td><?php echo $record->caller_name; ?></td>
                                            <td><?php echo $reasons[$record->reason_id]; ?></td>
                                            <td><?php echo $street_names[$record->street_id]; ?></td>
                                            <td><?php echo $technicians[$record->technician_id]; ?></td>
                                            <!--<td><?php // echo $record->status_id;     ?></td>-->
                                            <td><?php echo date("Y-m-d", strtotime($record->createdDtm)) ?></td>
                                            <td class="text-center">
                                                <a class="btn btn-sm btn-info edit_tech" href="<?php echo base_url() . $redirect_key; ?>breakDown/<?php echo $record->id; ?>"  title="Edit" ><i class="fa fa-pencil"></i></a>
                                                <span class="btn btn-sm btn-danger delete_btn" href="#" data-del_tbl="ims_breakdowns" data-del_id="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></span>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>
        </div>


        <!-- add absence type modal -->
        <!-- Modal -->
        <div id="addAbsenceModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form action="<?php echo base_url() . $redirect_key; ?>doAddBreakDown" method="post">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Create Break Down</h4> 
                            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
                        </div> 
                        <div class="modal-body">

                            <div class="form-group col-md-6">
                                <label class="control-label">Division</label>
                                <select name="division" class="form-control">
                                    <option value="Water">Water</option> 
                                    <option value="Electrical">Electrical</option>
                                    <option value="Both">Both</option>
                                </select> 
                            </div>


                            <div class="form-group col-md-6">
                                <label class="control-label">Status</label> 
                                <select name="status" class="form-control">
                                    <option value="Reported">Reported</option>
                                    <option value="Completed">Completed</option>
                                    <option value="On-Going">On-Going</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Connection Type</label> 
                                <select name="connection_type" class="form-control">
                                    <option value="Business">Business</option>
                                    <option value="Residential">Residential</option>
                                </select> 
                            </div>
                            <div class="form-group col-md-6">
                                <label class="control-label">Caller Name</label> 
                                <input name="caller_name" type="text" data-vv-rules="max:40" class="form-control" data-vv-id="2" aria-required="false" aria-invalid="false">
                                <!----></div>
                            <div class="form-group col-md-6">
                                <label class="control-label">Email Address</label>
                                <input name="email_street_name" type="text" data-vv-rules="max:25" class="form-control" data-vv-id="1" aria-required="false" aria-invalid="false">
                                <!---->
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Caller Region</label>
                                <select name="region" data-vv-rules="required|numeric|min:1" class="form-control" data-vv-id="3" aria-required="true" aria-invalid="false" required="">
                                    <option value="-1">--Select Region--</option>
                                    <?php
                                    foreach ($regions as $k => $val) {
                                        echo '<option value="' . $k . '">' . $val . '</option>';
                                    }
                                    ?>
                                </select> 
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Caller Address</label> 
                                <select name="street_name" data-vv-rules="required|numeric|min:1" class="form-control" data-vv-id="4" aria-required="true" aria-invalid="false" required="">
                                    <option value="-1">--Select Address--</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">House Number</label> 
                                <input name="house_number" type="text" data-vv-rules="max:40" class="form-control" data-vv-id="5" aria-required="false" aria-invalid="false"> 
                                <!---->
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Technician</label> 
                                <select name="technician" class="form-control" data-vv-id="6" aria-required="true" aria-invalid="false" required="">
                                    <option value="-1">--Select Technician--</option>
                                    <?php
                                    foreach ($technicians as $k => $val) {
                                        echo '<option value="' . $k . '">' . $val . '</option>';
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Please select Reason</label>
                                <select name="reason"  class="form-control" data-vv-id="7" aria-required="true" aria-invalid="false" required="">
                                    <option value="-1">--Select Reason--</option> 
                                    <?php
                                    foreach ($reasons as $k => $val) {
                                        echo '<option value="' . $k . '">' . $val . '</option>';
                                    }
                                    ?>
                                </select> 
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Street Name</label> 
                                <input name="street_name" type="text" data-vv-rules="max:40" class="form-control" data-vv-id="8" aria-required="false" aria-invalid="false">
                            </div>


                            <div class="form-group col-md-6">
                                <label class="control-label">Direction Note</label> 
                                <input name="direction_note" type="text" data-vv-rules="max:40" class="form-control" data-vv-id="9" aria-required="false" aria-invalid="false"> 
                                <!---->
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Meter No</label> 
                                <input name="meter_no" type="text" data-vv-rules="max:40" class="form-control" data-vv-id="10" aria-required="false" aria-invalid="false">
                                <!---->
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Cellular</label> 
                                <input name="cellular" type="text" data-vv-rules="max:40" class="form-control" data-vv-id="11" aria-required="false" aria-invalid="false">
                                <!---->
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Telephone</label> 
                                <input name="telephone" type="text" data-vv-rules="max:40" class="form-control" data-vv-id="12" aria-required="false" aria-invalid="false">
                                <!---->
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Reported Time To Technician</label> 
                                <input name="reported_time_to_technician" type="text" class="form-control"> 
                                <!---->
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Reported Date To Technician</label>
                                <input name="reported_date_to_technician" type="text" class="form-control datepicker"> 
                                <!---->
                            </div>

                            <div class="form-group col-md-6">
                                <label class="control-label">Problem</label>
                                <input name="problem" type="text" data-vv-rules="max:100" class="form-control" data-vv-id="13" aria-required="false" aria-invalid="false">
                                <!---->
                            </div>

                            <div class="col-sm-12 col-md-12 modal-footer">
                                <button type="submit" class="btn btn-primary">
                                    Create
                                </button> 
                                <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- update absence type modal-->
        <!-- Modal -->
        <div id="updateAbsenceModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form action="<?php echo base_url() . $redirect_key; ?>doUpdateTechnician" method="post">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Update Absence Type</h4>
                        </div>
                        <div class="modal-body">
                            <div class="modal-body">
                                <div class="form-group"><label>Name</label>
                                    <input type="text" class="form-control name_update" name="name" placeholder="Technician Name" >
                                </div>

                                <div class="form-group"><label for="ability">Ability</label>
                                    <input type="text" class="form-control ability_update" name="ability" placeholder="Ability" >
                                </div>
                            </div>
                            <input type="hidden" name="tech_id" id="technician_id" value="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-default">Update</button> &nbsp;
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" charset="utf-8"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />



<script type="text/javascript">
    jQuery(document).ready(function () {
        var table = $('#example').DataTable({ "order": [[ 0, 'desc' ]] });
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            startDate: '-7d',
            autoclose: true
        });

        $('.edit_tech').on('click', function () {
            var tech_id = $(this).data('tech_id');

            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getTechnicianDetails'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    tech_id: tech_id,
                },
                success: function (data) {
                    $('#updateAbsenceModal .name_update').val(data.name);
                    $('#updateAbsenceModal .ability_update').val(data.ability);

                    $('#updateAbsenceModal #technician_id').val(data.id);

//                    $('#updateAbsenceModal #update_node_id').val(data.node_id);
                    $('#updateAbsenceModal').modal('show');
                }
            });
        });


        $('.delete_btn').on('click', function () {
            var del_id = $(this).data('del_id');
            var del_tbl = $(this).data('del_tbl');
            var del_this = $(this);
            var x = confirm('are you sure you want to delete this ?');
            if (x) {
                $.ajax({
                    url: "<?php echo base_url() . $redirect_key . 'deleteRow'; ?>",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        del_id: del_id,
                        del_tbl: del_tbl,
                    },
                    success: function (data) {
                        del_this.parent().parent().hide();
                    }
                });
            }
        });
    });
</script>
