<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-cog"></i> Settings
            <small>Send To, Department</small>
        </h1>
        <div class="col-md-3 pull-right right">
            <?php
            $error = $this->session->flashdata('error');
            if ($error) {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
            <?php } ?>
            <?php
            $success = $this->session->flashdata('success');
            if ($success) {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php } ?>
        </div>
    </section>
    <section class="content">


        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#street_name_tab">Send To</a></li>
            <li><a data-toggle="tab" href="#department_tab">Department</a></li>
        
        </ul>

        <div class="tab-content">
            <div id="street_name_tab" class="tab-pane fade in active">

                <div class="Send_To_Area"> 
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <div class="form-group">
                                <span class="btn btn-primary add_street_name" data-toggle="modal" data-target="#addSendToModal"><i class="fa fa-plus"></i> Add New</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"></h3>
                                    <div class="box-tools">

                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" id="example">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Department</th>
                                                <th>Created On</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($send_tos)) {
                                                foreach ($send_tos->result() as $record) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $record->fname; ?></td>
                                                        <td><?php echo $record->lname; ?></td>
                                                        <td><?php echo $departments_dp[$record->department_id]; ?></td>
                                                        <td><?php echo date("d-m-Y", strtotime($record->createdDtm)) ?></td>
                                                        <td class="text-center">
                                                            <span class="btn btn-sm btn-info update_sendTo"  title="Edit" data-curr_dept="<?php echo $record->department_id; ?>" data-send_to_id="<?php echo $record->id; ?>"><i class="fa fa-pencil"></i></span>
                                                            <span class="btn btn-sm btn-danger delete_btn" href="#" data-del_tbl="ims_send_to" data-del_id="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></span>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>
                        <!-- add department modal -->
                        <!-- Modal -->
                        <div id="addSendToModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <form action="<?php echo base_url() . $redirect_key; ?>doAddSendTo" method="post">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add New Send To</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group"><label for="department">Department</label>
                                                <select name="department" class="department_dp form-control">
                                                    <?php
                                                    if (!empty($departments)) {
                                                        foreach ($departments->result() as $record) {
                                                            ?>
                                                            <option value="<?php echo $record->id; ?>"> <?php echo $record->department; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group"><label>First Name</label>
                                                <input type="text" class="form-control" name="fname" placeholder="First Name" >
                                            </div>
                                            <div class="form-group"><label>Last Name</label>
                                                <input type="text" class="form-control" name="lname" placeholder="Last Name" >
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-default">Create</button> &nbsp;
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- update department modal-->
                        <!-- Modal -->
                        <div id="updateSend_ToModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <form action="<?php echo base_url() . $redirect_key; ?>doUpdateSendTo" method="post">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update Send To</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group"><label>First Name</label>
                                                <input type="text" class="form-control update_fname" name="fname" placeholder="First Name" >
                                            </div>
                                            <div class="form-group"><label>Last Name</label>
                                                <input type="text" class="form-control update_lname" name="lname" placeholder="Last Name" >
                                            </div>

                                            <div class="form-group"><label for="department">Department</label>
                                                <select name="department_id" class="department_dp_update form-control">

                                                </select>
                                            </div>
                                            <input type="hidden" id="update_send_to_id" name="send_to_id" value="">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info">Update</button> &nbsp;
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div id="department_tab" class="tab-pane fade in">

                <div class="Department_Area"> 
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            <div class="form-group">
                                <span class="btn btn-primary" data-toggle="modal" data-target="#addDepartmentModal"><i class="fa fa-plus"></i> Add New</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title"></h3>
                                    <div class="box-tools">

                                    </div>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive no-padding">
                                    <table class="table table-hover" id="example">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Created On</th>
                                                <th class="text-center">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            if (!empty($departments)) {
                                                foreach ($departments->result() as $record) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $record->department; ?></td>
                                                        <td><?php echo date("d-m-Y", strtotime($record->createdDtm)) ?></td>
                                                        <td class="text-center">
                                                            <span class="btn btn-sm btn-info update_department"  title="Edit" data-tech_id="<?php echo $record->id; ?>"><i class="fa fa-pencil"></i></span>
                                                            <span class="btn btn-sm btn-danger delete_btn" href="#" data-del_tbl="ims_department" data-del_id="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></span>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>

                                </div><!-- /.box-body -->

                            </div><!-- /.box -->
                        </div>
                        <!-- add department modal -->
                        <!-- Modal -->
                        <div id="addDepartmentModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <form action="<?php echo base_url() . $redirect_key; ?>doAddDepartment" method="post">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Add New Department</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group"><label>Department</label>
                                                <input type="text" class="form-control" name="department" placeholder="Department Name" >
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-default">Create</button> &nbsp;
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- update department modal-->
                        <!-- Modal -->
                        <div id="updateDepartmentModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <form action="<?php echo base_url() . $redirect_key; ?>doUpdateDepartment" method="post">
                                    <!-- Modal content-->
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Update Department</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="modal-body">
                                                <div class="form-group"><label>Department</label>
                                                    <input type="text" class="form-control department_update" name="department" placeholder="Department Name" >
                                                </div>
                                            </div>
                                            <input type="hidden" name="resion_id" id="update_department_id" value="">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-default">Update</button> &nbsp;
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
         
        </div>


    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var table = $('#example').DataTable({"order": [[0, 'desc']]});


        $('.update_sendTo').on('click', function () {
            var send_to_id = $(this).data('send_to_id');
            var curr_dept = $(this).data('curr_dept');
            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getDepartmentDp'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    send_to_id: send_to_id,
                    curr_dept: curr_dept
                },
                success: function (data) {
                    $('#updateSend_ToModal .department_dp_update').html(data.department_dp);
                }
            });
            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getSend_ToDetails'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    send_to_id: send_to_id,
                },
                success: function (data) {
                    $('#updateSend_ToModal .update_fname').val(data.fname);
                    $('#updateSend_ToModal .update_lname').val(data.lname);
                    $('#updateSend_ToModal #update_send_to_id').val(data.id);
                    $('#updateSend_ToModal').modal('show');
                }
            });
        });
        $('.update_department').on('click', function () {
            var department_id = $(this).data('department_id');
            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getDepartmentDetails'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    department_id: department_id,
                },
                success: function (data) {
                    $('#updateDepartmentModal .department_update').val(data.department);
                    $('#updateDepartmentModal #update_department_id').val(data.id);
                    $('#updateDepartmentModal').modal('show');
                }
            });
        });
        $('.update_reason').on('click', function () {
            var department_id = $(this).data('department_id');
            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getReasonDetails'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    department_id: department_id,
                },
                success: function (data) {
                    $('#updateReasonModal .reason_update').val(data.department);
                    $('#updateReasonModal #update_reason_id').val(data.id);
                    $('#updateReasonModal').modal('show');
                }
            });
        });

        $('.delete_btn').on('click', function () {
            var del_id = $(this).data('del_id');
            var del_tbl = $(this).data('del_tbl');
            var del_this = $(this);
            var x = confirm('are you sure you want to delete this ?');
            if (x) {
                $.ajax({
                    url: "<?php echo base_url() . $redirect_key . 'deleteRow'; ?>",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        del_id: del_id,
                        del_tbl: del_tbl,
                    },
                    success: function (data) {
                        del_this.parent().parent().hide();
                    }
                });
            }
        });
    });
</script>
