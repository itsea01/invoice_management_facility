<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?php if (isset($edit_breakdown) && $edit_breakdown > 0) { ?>
                <i class="fa fa-file-text-o"></i>
            <?php } else { ?>
                <i class="fa fa-file-o"></i>
            <?php } ?>

            Invoice Manager
            <small><?php
                echo $dashboardTitle;
                ?></small>
        </h1>
    </section>
    <style>
        .modal-dialog{
            width: 900px;
            height: 810px;
        }
        .modal-content{
            height: 100%;
        }
    </style>
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"><?php echo $dashboardTitle; ?></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <?php
                        $edit_bd = 0;
                        if (isset($edit_breakdown) && $edit_breakdown > 0) {
                            $edit_bd = $edit_form;
                        }
                        ?>
                        <form action="<?php echo base_url() . $redirect_key; ?>doAddInvoice" method="post">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-body">
                                    <input type="hidden" name="edit_form" value="<?php echo $edit_bd; ?>">
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Invoice No</label> 
                                        <input name="invoice_no" type="text" value="<?php echo $invoice_data->invoice_no; ?>" class="form-control" data-vv-id="2" aria-required="false" aria-invalid="false">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">Invoice Date</label>
                                        <div class="input-group">
                                            <input name="invoice_date" value="<?php echo $invoice_data->invoice_date; ?>" placeholder="yyyy-mm-dd" type="text" class="form-control datepicker1" autocomplete="off"> 
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="control-label">PO No</label> 
                                        <input name="form_no" type="text" value="<?php echo $invoice_data->form_no; ?>" class="form-control" data-vv-id="2" aria-required="false" aria-invalid="false" placeholder="PO Number">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label mandatory">Vendor</label>
                                        <select name="vendor_id" class="form-control mandatory_input" data-vv-id="3" aria-required="true" aria-invalid="false" required="">
                                            <?php
                                            foreach ($vendors as $k => $val) {
                                                $selected = '';
                                                if ($invoice_data->vendor_id == $k) {
                                                    $selected = ' Selected';
                                                }
                                                echo '<option value="' . $k . '"' . $selected . '>' . $val . '</option>';
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label mandatory">Currency</label>
                                        <?php
                                        // $currancy = array('USD', 'GBP', 'EUR');
                                        ?>
                                        <select name="currancy" class="form-control ">
                                            <?php
                                            foreach ($currancy as $val) {
                                                $selected = '';
                                                if ($invoice_data->currancy == $val) {
                                                    $selected = ' Selected';
                                                }
                                                echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Invoice Amount</label> 
                                        <input name="invoice_amount" type="text" value="<?php echo $invoice_data->invoice_amount; ?>" class="form-control" placeholder="Invoice Amount" aria-required="false" aria-invalid="false"> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Please select Category</label> 

                                        <select name="category" class="form-control">
                                            <?php
                                            foreach ($categories as $val) {
                                                $selected = '';
                                                if ($invoice_data->category == $val) {
                                                    $selected = ' Selected';
                                                }
                                                echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group col-md-6">

                                        <label class="control-label">Date Entered</label>
                                        <div class="input-group">
                                            <input name="date_entered" value="<?php echo $invoice_data->date_entered; ?>" placeholder="yyyy-mm-dd" type="text" class="form-control datepicker" autocomplete="off"> 
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label mandatory">Please Select Send To</label>
                                        <select name="send_to_id" class="form-control mandatory_input" data-vv-id="3" aria-required="true" aria-invalid="false" required="">
                                            <option>--Select Send To--</option>
                                            <?php
                                            foreach ($send_tos as $k => $val) {
                                                $selected = '';
                                                if ($invoice_data->send_to_id == $k) {
                                                    $selected = ' Selected';
                                                }
                                                echo '<option value="' . $k . '"' . $selected . '>' . $val . '</option>';
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Send To Date</label>
                                        <div class="input-group">
                                            <input name="send_to_date" value="<?php echo $invoice_data->send_to_date; ?>" placeholder="yyyy-mm-dd" type="text" class="form-control datepicker" autocomplete="off"> 
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Date Returned</label>
                                        <div class="input-group">
                                            <input name="date_returned" value="<?php echo $invoice_data->date_returned; ?>" placeholder="yyyy-mm-dd" type="text" class="form-control datepicker" autocomplete="off"> 
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Status</label> 
                                        <?php ?>
                                        <select name="status" class="form-control" required="">
                                            <option>--Select Status--</option>
                                            <?php
                                            foreach ($status_types as $val) {
                                                $selected = '';
                                                if ($invoice_data->status == $val) {
                                                    $selected = ' Selected';
                                                }
                                                echo '<option value="' . $val . '" ' . $selected . '>' . $val . '</option>';
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label class="control-label">Remark</label> 
                                        <input name="remark" type="text" value="<?php echo $invoice_data->remark; ?>" class="form-control" data-vv-id="2" aria-required="false" placeholder="Remark">
                                    </div>
									 <div class="form-group col-md-12">
                                        <label class="control-label">Note</label> 
                                        <input name="note" type="text" value="<?php echo $invoice_data->note; ?>" class="form-control" aria-required="false" placeholder="Note">
                                    </div>
                                    <div class="col-sm-12 col-md-12 modal-footer">
                                        <button type="submit" class="btn btn-primary submit_btn">
                                            <?php
                                            if ($edit_bd) {
                                                echo 'Update';
                                            } else {
                                                echo 'Create';
                                            }
                                            ?>
                                        </button> 
                                        <button type="button" onclick="window.history.back()" class="btn btn-secondary">Close</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" charset="utf-8"></script>
<!--<script type="text/javascript" src="http://jdewit.github.io/bootstrap-timepicker/js/bootstrap-timepicker.js" charset="utf-8"></script>-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />



<script type="text/javascript">
    jQuery(document).ready(function () {
        var table = $('#example').DataTable();

        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            startDate: '-7d',
            autoclose: true
        });

        $('.datepicker1').datepicker({
            format: 'yyyy-mm-dd',

            autoclose: true
        });
        //        $('.timepicker').timepicker();
        //        $('#timepicker1').timepicker();
        //
        //        $('#timepicker1').on('changeTime.timepicker', function (e) {
        //           // $('#timeDisplay').text(e.time.value);
        //        });

        //        $('.mandatory_input').on('change', function () {
        //            var found = 0;
        //            // console.log('change');
        //            $.each($('.mandatory_input'), function () {
        //                var x = $(this).val();
        //                console.log(x);
        //                if (x == '' || x === undefined || x == -1) {
        //                    $('.submit_btn').attr('disabled', 'true');
        //                    found = 1;
        //
        //                }
        //            });
        //            if (found == 0) {
        //                $('.submit_btn').removeAttr('disabled');
        //            }
        //        });
        $('.edit_tech').on('click', function () {
            var tech_id = $(this).data('tech_id');

            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getTechnicianDetails'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    tech_id: tech_id,
                },
                success: function (data) {
                    $('#updateAbsenceModal .name_update').val(data.name);
                    $('#updateAbsenceModal .ability_update').val(data.ability);

                    $('#updateAbsenceModal #technician_id').val(data.id);

                    //                    $('#updateAbsenceModal #update_node_id').val(data.node_id);
                    $('#updateAbsenceModal').modal('show');
                }
            });
        });


        $('.delete_btn').on('click', function () {
            var del_id = $(this).data('del_id');
            var del_tbl = $(this).data('del_tbl');
            var del_this = $(this);
            var x = confirm('are you sure you want to delete this ?');
            if (x) {
                $.ajax({
                    url: "<?php echo base_url() . $redirect_key . 'deleteRow'; ?>",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        del_id: del_id,
                        del_tbl: del_tbl,
                    },
                    success: function (data) {
                        del_this.parent().parent().hide();
                    }
                });
            }
        });
    });
</script>
