<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-users"></i> Trackings
            <small>Add, Edit, Delete</small>
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="col-md-3 pull-left left">
                    <?php
                    $error = $this->session->flashdata('error');
                    if ($error) {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $this->session->flashdata('error'); ?>                    
                        </div>
                    <?php } ?>
                    <?php
                    $success = $this->session->flashdata('success');
                    if ($success) {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <?php echo $this->session->flashdata('success'); ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="form-group">
                    <span class="btn btn-primary" data-toggle="modal" data-target="#addTrackingModal"><i class="fa fa-plus"></i> Add New</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Trackings List</h3>
                        <div class="box-tools">

                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover" id="example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Invoice No</th>
                                    <th>Form No</th>
                                    <th>Send To</th>
                                    <th>Date Send To</th>
                                    <th>Date Returned</th>
                                    <th>Created On</th>
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($trackings)) {
                                    foreach ($trackings->result() as $record) {
                                        ?>
                                        <tr>
                                            <td><?php echo $record->id; ?></td>
                                            <td><?php echo $record->invoice_no; ?></td>
                                            <td><?php echo $record->invoice_form; ?></td>
                                            <td><?php echo $vendors[$record->send_to_id]; ?></td>
                                            <td><?php echo date("Y-m-d", strtotime($record->send_to_date)); ?></td>
                                            <td><?php echo date("Y-m-d", strtotime($record->returned_date)); ?></td>
                                            <td><?php echo date("Y-m-d", strtotime($record->createdDtm)); ?></td>
                                            <td class="text-center">
                                                <span class="btn btn-sm btn-info edit_tracking"  title="Edit" data-tracking_id="<?php echo $record->id; ?>"><i class="fa fa-pencil"></i></span>
                                                <span class="btn btn-sm btn-danger delete_btn" href="#" data-del_tbl="ims_trackings" data-del_id="<?php echo $record->id; ?>" title="Delete"><i class="fa fa-trash"></i></span>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>

                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div>
        </div>


        <!-- add absence type modal -->
        <!-- Modal -->
        <div id="addTrackingModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form action="<?php echo base_url() . $redirect_key; ?>doAddTracking" method="post">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Add Tracking</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group"><label>Select Form</label>
                                <select class="form-control location" name="invoice_data">
                                    <?php
                                    if (!empty($invoices)) {
                                        foreach ($invoices->result() as $record) {
                                            ?>
                                            <option value="<?php echo $record->id . '_' . $record->invoice_no . '_' . $record->form_no; ?>"> <?php echo $record->invoice_no . ' - ' . $record->form_no; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="form-group"><label for="location">Select Send To</label>
                                <select class="form-control location" name="send_to_id">
                                    <?php
                                    if (!empty($sendtos)) {
                                        foreach ($sendtos->result() as $record) {
                                            ?>
                                            <option value="<?php echo $record->id; ?>"> <?php echo $record->fname . ' ' . $record->lname; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group"><label for="send_to_date">Send To Date</label>
                                <input type="text" class="form-control datepicker" autocomplete="off" name="send_to_date" placeholder="Send To Date" >
                            </div>
                            <div class="form-group"><label for="returned_date">Returned Date</label>
                                <input type="text" class="form-control datepicker" autocomplete="off" name="returned_date" placeholder="Returned Date" >
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-default">Create</button> &nbsp;
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- update absence type modal-->
        <!-- Modal -->
        <div id="updateTrackingModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form action="<?php echo base_url() . $redirect_key; ?>doUpdateTracking" method="post">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Update Tracking</h4>
                        </div>
                        <div class="modal-body">
                            <div class="modal-body">


                                <div class="form-group">
                                    <label>Select Form</label>
                                    <select class="form-control update_invoice_data" name="invoice_data">
                                        <?php
                                        if (!empty($invoices)) {
                                            foreach ($invoices->result() as $record) {
                                                ?>
                                                <option value="<?php echo $record->id . '_' . $record->invoice_no . '_' . $record->form_no; ?>"> <?php echo $record->invoice_no . ' - ' . $record->form_no; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group"><label for="update_send_to_id">Select Send To</label>
                                    <select class="form-control update_send_to_id" name="send_to_id">
                                        <?php
                                        if (!empty($sendtos)) {
                                            foreach ($sendtos->result() as $record) {
                                                ?>
                                                <option value="<?php echo $record->id; ?>"> <?php echo $record->fname . ' ' . $record->lname; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group"><label for="send_to_date">Send To Date</label>
                                    <input type="text" class="form-control datepicker update_send_to_date" autocomplete="off" name="send_to_date" placeholder="Send To Date" >
                                </div>
                                <div class="form-group"><label for="returned_date">Returned Date</label>
                                    <input type="text" class="form-control datepicker update_returned_date" autocomplete="off" name="returned_date" placeholder="Returned Date" >
                                </div>

                            </div>
                            <input type="hidden" name="tracking_id" id="tracking_id" value="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-default">Update</button> &nbsp;
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js" charset="utf-8"></script>
<!--<script type="text/javascript" src="http://jdewit.github.io/bootstrap-timepicker/js/bootstrap-timepicker.js" charset="utf-8"></script>-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
    jQuery(document).ready(function () {
        var table = $('#example').DataTable({"order": [[0, 'desc']]});
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            startDate: '-7d',
            autoclose: true
        });
        $('.edit_tracking').on('click', function () {
            var tracking_id = $(this).data('tracking_id');
            $.ajax({
                url: "<?php echo base_url() . $redirect_key . 'getTrakcingDetails'; ?>",
                type: 'POST',
                dataType: 'JSON',
                data: {
                    tracking_id: tracking_id,
                },
                success: function (data) {
                    $('#updateTrackingModal .update_invoice_data').val(data.invoice_data);
                    $('#updateTrackingModal .update_send_to_id').val(data.send_to_id);
                    $('#updateTrackingModal .update_send_to_date').val(data.send_to_date);
                    $('#updateTrackingModal .update_returned_date').val(data.returned_date);
                    $('#updateTrackingModal #tracking_id').val(data.id);

                    $('#updateTrackingModal').modal('show');
                }
            });
        });


        $('.delete_btn').on('click', function () {
            var del_id = $(this).data('del_id');
            var del_tbl = $(this).data('del_tbl');
            var del_this = $(this);
            var x = confirm('are you sure you want to delete this ?');
            if (x) {
                $.ajax({
                    url: "<?php echo base_url() . $redirect_key . 'deleteRow'; ?>",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {
                        del_id: del_id,
                        del_tbl: del_tbl,
                    },
                    success: function (data) {
                        del_this.parent().parent().hide();
                    }
                });
            }
        });
    });
</script>
